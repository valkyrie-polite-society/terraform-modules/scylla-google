resource "google_compute_instance" "scylla" {
  count = var.cluster_size

  name         = "${var.cluster_name}-${count.index}"
  machine_type = var.machine_type
  zone         = var.zone

  tags = [var.cluster_name]

  boot_disk {
    initialize_params {
      image = data.google_compute_image.scylla.self_link
    }
  }

  attached_disk {
    device_name = "scylla-data"
    mode        = "READ_WRITE"
    source      = google_compute_disk.data[count.index].self_link
  }

  attached_disk {
    device_name = "scylla-commitlog"
    mode        = "READ_WRITE"
    source      = google_compute_disk.commitlog[count.index].self_link
  }

  attached_disk {
    device_name = "scylla-view-hints"
    mode        = "READ_WRITE"
    source      = google_compute_disk.view_hints[count.index].self_link
  }

  network_interface {
    network = var.network
    access_config {
      nat_ip = google_compute_address.public[count.index].address
    }
  }

  metadata = {
    "user-data" = templatefile("${path.module}/cloud-init.yaml", {
      consul_ca_pem_b64  = base64encode(data.terraform_remote_state.ca.outputs.root_ca_certificate_pem)
      consul_tls_key_b64 = base64encode(tls_private_key.consul[count.index].private_key_pem)
      consul_tls_crt_b64 = base64encode(join("\n", [
        tls_locally_signed_cert.consul[count.index].cert_pem,
        data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem,
        data.terraform_remote_state.ca.outputs.root_ca_certificate_pem,
      ]))
      io_properties = base64encode(file("${path.module}/files/io_properties.yaml"))
      io_conf = base64encode(file("${path.module}/files/io.conf"))

      fstab = base64encode(file("${path.module}/files/fstab"))
      chown_mounts = base64encode(file("${path.module}/files/scylla-chown-mounts.service"))
      required_units = base64encode(file("${path.module}/files/required-units.conf"))
      restart = base64encode(file("${path.module}/files/restart.conf"))
      start_limit = base64encode(file("${path.module}/files/start-limit.conf"))
    })
  }

  service_account {
    scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}
