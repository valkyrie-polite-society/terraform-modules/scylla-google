# Scylla on Google Cloud Platform

TODO: Document the MVP

## What's cool

### Modern filesystem management using systemd

In newer Linux distributions, systemd provides a simple way to assure the existence of a file system and make sure that it grows if the disk grows.
In `/etc/fstab`:

```
LABEL=cloudimg-rootfs                     /                          ext4  defaults                                   0 1
LABEL=UEFI                                /boot/efi                  vfat  defaults                                   0 1
/dev/disk/by-id/google-scylla-commitlog   /var/lib/scylla/commitlog  xfs   defaults,x-systemd.makefs,x-systemd.growfs 0 2
/dev/disk/by-id/google-scylla-view-hints  /var/lib/scylla/view_hints xfs   defaults,x-systemd.makefs,x-systemd.growfs 0 2
/dev/disk/by-id/google-scylla-data        /var/lib/scylla/data       xfs   defaults,x-systemd.makefs,x-systemd.growfs 0 2
```

Then systemd-fstab-generator "just does the right thing",

```
adriennecohea_gmail_com@asgard-scylla-1:~$ sudo systemctl list-units | grep makefs
  systemd-makefs@dev-disk-by\x2did-google\x2dscylla\x2dcommitlog.service                       loaded active     exited          Make File System on /dev/disk/by-id/google-scylla-commitlog                  
  systemd-makefs@dev-disk-by\x2did-google\x2dscylla\x2ddata.service                            loaded active     exited          Make File System on /dev/disk/by-id/google-scylla-data                       
  systemd-makefs@dev-disk-by\x2did-google\x2dscylla\x2dview\x2dhints.service                   loaded active     exited          Make File System on /dev/disk/by-id/google-scylla-view-hints
```

```
adriennecohea_gmail_com@asgard-scylla-1:~$ sudo systemctl list-units | grep growfs
  systemd-growfs@var-lib-scylla-commitlog.service                                              loaded active     exited          Grow File System on /var/lib/scylla/commitlog                                
  systemd-growfs@var-lib-scylla-data.service                                                   loaded active     exited          Grow File System on /var/lib/scylla/data                                     
  systemd-growfs@var-lib-scylla-view_hints.service                                             loaded active     exited          Grow File System on /var/lib/scylla/view_hints
```

```
adriennecohea_gmail_com@asgard-scylla-1:~$ sudo systemctl list-units --type=mount | grep scylla
  var-lib-scylla-commitlog.mount        loaded active mounted /var/lib/scylla/commitlog                    
  var-lib-scylla-data.mount             loaded active mounted /var/lib/scylla/data                         
  var-lib-scylla-view_hints.mount       loaded active mounted /var/lib/scylla/view_hints
```
