output "external_ips" {
  value = google_compute_address.public.*.address
}

output "external_endpoints" {
  value = formatlist("cassandra://%s:9042", google_compute_address.public.*.address)
}
