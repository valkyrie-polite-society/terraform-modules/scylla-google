resource "google_compute_disk" "data" {
  count = var.cluster_size

  name = "scylla-${count.index}-data"
  type = "pd-ssd"
  size = 200
  zone = var.zone
}

resource "google_compute_disk" "commitlog" {
  count = var.cluster_size

  name = "scylla-${count.index}-commitlog"
  type = "pd-ssd"
  size = 20
  zone = var.zone
}

resource "google_compute_disk" "view_hints" {
  count = var.cluster_size

  name = "scylla-${count.index}-view-hints"
  type = "pd-ssd"
  size = 50
  zone = var.zone
}
