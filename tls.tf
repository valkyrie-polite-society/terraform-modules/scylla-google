# scylla TLS certificates

resource "tls_private_key" "scylla" {
  count = var.cluster_size

  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "scylla" {
  count = var.cluster_size

  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.scylla[count.index].private_key_pem

  subject {
    common_name  = "Scylla Server"
    organization = "Valkyrie Polite Society"
  }

  ip_addresses = [
    "127.0.0.1",
    google_compute_address.public[count.index].address,
  ]

  dns_names = [
    "localhost",
    "scylla.service.consul",
    trimsuffix(google_dns_record_set.scylla.name, "."),
  ]
}

resource "tls_locally_signed_cert" "scylla" {
  count = var.cluster_size

  ca_key_algorithm   = "ECDSA"
  cert_request_pem   = tls_cert_request.scylla[count.index].cert_request_pem
  ca_private_key_pem = data.terraform_remote_state.ca.outputs.intermediate_ca_private_key_pem
  ca_cert_pem        = data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem

  validity_period_hours = 24 * 69 # Nice

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
  ]
}

# Consul TLS certificates

resource "tls_private_key" "consul" {
  count = var.cluster_size

  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "consul" {
  count = var.cluster_size

  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.consul[count.index].private_key_pem

  subject {
    common_name  = "Consul Client"
    organization = "Valkyrie Polite Society"
  }

  ip_addresses = [
    "127.0.0.1",
    google_compute_address.public[count.index].address,
  ]

  dns_names = [
    "localhost",
    "client.${var.consul_datacenter}.consul",
  ]
}

resource "tls_locally_signed_cert" "consul" {
  count = var.cluster_size

  ca_key_algorithm   = "ECDSA"
  cert_request_pem   = tls_cert_request.consul[count.index].cert_request_pem
  ca_private_key_pem = data.terraform_remote_state.ca.outputs.intermediate_ca_private_key_pem
  ca_cert_pem        = data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem

  validity_period_hours = 24 * 69 # Nice

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
  ]
}

data "terraform_remote_state" "ca" {
  backend = "gcs"

  config = {
    bucket = var.certificate_authority_remote_state_bucket
    prefix = var.certificate_authority_remote_state_prefix
  }
}
